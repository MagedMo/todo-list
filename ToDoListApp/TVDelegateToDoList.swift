//
//  TVDelegateToDoList.swift
//  ToDoListApp
//
//  Created by Magid on 10/10/18.
//  Copyright © 2018 Magid. All rights reserved.
//

import Foundation
import UIKit
import CoreData
extension ToDoListVC:UITableViewDelegate
{
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) { //7
        if editingStyle == .delete {
            let task = Task[indexPath.row]
            context.delete(task)
            (UIApplication.shared.delegate as! AppDelegate).saveContext()
            
            let fetchRequst = NSFetchRequest<NSManagedObject>(entityName: "Tasks")
            do{
                Task =  try context.fetch(fetchRequst)
            }
            catch {
                print("Fetching Failed")
            }
        }
        tableView.reloadData()
    }


    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.separatorInset = .zero
        cell.layoutMargins = .zero
    }
    
    
}
