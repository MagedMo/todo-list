//
//  TVDataSourceToDOList.swift
//  ToDoListApp
//
//  Created by Youxel on 10/10/18.
//  Copyright © 2018 Magid. All rights reserved.
//

import Foundation
import UIKit
import CoreData
extension ToDoListVC:UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Task.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:ToDoCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ToDoCell
        let TaskData:Tasks = Task[indexPath.row] as! Tasks
        
        cell.TaskTitle.text = TaskData.value(forKey: "taskName") as! String?
        cell.TaskDisc.text = TaskData.value(forKey: "taskDisc") as! String?
        cell.TaskDate.text = String(describing: TaskData.taskData!)//value(forKey: "taskDate") as! String?
        
        return cell
    }
    
    
}
