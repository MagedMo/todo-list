//
//  ViewController.swift
//  ToDoListApp
//
//  Created by Magid on 10/8/18.
//  Copyright © 2018 Magid. All rights reserved.
//

import UIKit
import CoreData
class LogInVC: UIViewController {
    
    
    @IBOutlet weak var WelcomLa: UILabel!
    @IBOutlet weak var UserImage: UIImageView!
    @IBOutlet weak var UserNameText: UITextField!
    @IBOutlet weak var UserPasswordText: UITextField!
    
    @IBOutlet weak var loginBuT: UIButton!
    
    @IBOutlet weak var ToDoIcon: UIButton!
    @IBOutlet weak var ExitIcon: UIButton!
    
    @IBOutlet weak var reg: UIButton!
    
    var User:[Users] = []
    let defaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let UserName = defaults.string(forKey: "UserName")
        {
            UserPanel()
        }else
        {
            UserPanelOff()
        }
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    
    
    @IBAction func login(_ sender: UIButton) {
        
        if !((UserNameText.text?.isEmpty)!) && !((UserPasswordText.text?.isEmpty)!)
        {
            let fetchRequst:NSFetchRequest<Users> = Users.fetchRequest()
            do{
                self.User = try context.fetch(fetchRequst)
                
                let User = self.User.first
                if  User?.value(forKey: "name")as! String == UserNameText.text! && User?.value(forKey: "password")as? String == UserPasswordText.text
                {
                    UserPanel()
                    UserImage.image = User?.image as! UIImage
                    /// Save Data to User Defults
                    DispatchQueue.global().async {
                        self.defaults.set(User?.name, forKey: "UserName")
                    }
                                    }
                else {print("Error User Name Or Pasword Not valide")
                }
                
                
                
            }catch let Error as NSError{
                print ( "Error \(Error)")
            }
            
            
        }else {
            print ("Error User Or PassWord Is impty")
            
        }
        

        
    }
    
    
    @IBAction func LogOutFun(_ sender: Any)
    {
        //LogOut And Delete UserDeflts Data and show Log in Fourm
        
        UserPanelOff()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }


}

