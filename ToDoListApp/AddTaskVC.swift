//
//  AddTaskVC.swift
//  ToDoListApp
//
//  Created by Magid on 10/10/18.
//  Copyright © 2018 Magid. All rights reserved.
//

import UIKit
import CoreData
class AddTaskVC: UIViewController {

    @IBOutlet weak var TaskName: UITextField!
    @IBOutlet weak var TaskDisc: UITextView!
    
    var User = [Users]()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    
    @IBAction func SaveTask(_ sender: Any) {
        
        
        let entity = NSEntityDescription.entity(forEntityName: "Tasks", in: context)
        let UserData:Tasks = NSManagedObject(entity: entity!, insertInto: context) as! Tasks
        UserData.taskName = TaskName.text //setValue(TaskName.text, forKey: "taskName")
        UserData.taskDisc = TaskDisc.text //setValue(TaskDisc.text, forKey: "taskDisc")
        UserData.taskData = NSDate() //setValue(NSDate(), forKey: "taskDate")        
        TaskName.text = ""
        TaskDisc.text = ""
        print ("Saved")
          do{
            try! context.save()
        }
        catch let error as NSError{ print("Error to Save \(error)")}
        
        
    }
    
    

}
