//
//  ToDoListVC.swift
//  ToDoListApp
//
//  Created by Magid on 10/10/18.
//  Copyright © 2018 Magid. All rights reserved.
//

import UIKit
import CoreData
class ToDoListVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var Task:[NSManagedObject] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        FetchData()
        tableView.delegate = self
        tableView.dataSource = self
                tableView.reloadData()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
           
      
    func FetchData()
    {
        let fetchRequst = NSFetchRequest<NSManagedObject>(entityName: "Tasks")
        do{
            Task =  try context.fetch(fetchRequst)
                    }catch let Error as NSError{
            print ( "Error \(Error)")
        }
    }
    
}
