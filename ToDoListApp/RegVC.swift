//
//  RegVC.swift
//  ToDoListApp
//
//  Created by Magid on 10/9/18.
//  Copyright © 2018 Magid. All rights reserved.
//

import UIKit
import CoreData

class RegVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var UserImage: UIImageView!
    @IBOutlet weak var UserName: UITextField!
    @IBOutlet weak var UserPass: UITextField!
    
     var imagePicker:UIImagePickerController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imagePicker = UIImagePickerController()
        imagePicker.delegate = self

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func SignUp(_ sender: Any)
    {
        
        let entity = NSEntityDescription.entity(forEntityName: "Users", in: context)
        let UserData = NSManagedObject(entity: entity!, insertInto: context)
        UserData.setValue(UserName.text, forKey: "name")
        UserData.setValue(UserPass.text, forKey: "password")
        UserData.setValue(UserImage.image, forKey: "image")
        UserName.text = ""
        UserPass.text = ""
        print ("Saved")
        do{
            try! context.save()
        }
        catch let error as NSError{ print("Error to Save \(error)")}
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        DispatchQueue.main.async {
            if let image = info[UIImagePickerControllerOriginalImage] as? UIImage{
                self.UserImage.image=image
            }
            self.imagePicker.dismiss(animated: true, completion: nil)

        }
        
    }

    @IBAction func GetUserImage(_ sender: Any) {
        
       present(imagePicker, animated: true, completion: nil)
    }

}
