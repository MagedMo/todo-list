//
//  helperMethod.swift
//  ToDoListApp
//
//  Created by Magid on 10/9/18.
//  Copyright © 2018 Magid. All rights reserved.
//

import Foundation
import UIKit

extension LogInVC
{
    // User Panal Func
    func UserPanel()
    {
    UIView.animate(withDuration: (1.0), animations: {
        self.UserNameText.isHidden = true
        self.UserPasswordText.isHidden = true
        self.loginBuT.isHidden = true
        self.reg.isHidden = true
        self.WelcomLa.text = "Welcome  \(self.UserNameText.text!)"
        //self.UserImage.image = self.defaults.value(forKey: "image") as? UIImage
        self.ToDoIcon.isHidden = false
        self.ExitIcon.isHidden = false
            
        })
    
    }

    func UserPanelOff()
    {
        
    UIView.animate(withDuration: (1.0), animations: {
        self.UserNameText.isHidden = false
        self.UserPasswordText.isHidden = false
        self.loginBuT.isHidden = false
        self.reg.isHidden = false
        self.WelcomLa.text = "Welcome Geust"
        self.UserImage.image = UIImage(named: "default_profile")
        self.ToDoIcon.isHidden = true
        self.ExitIcon.isHidden = true
        self.defaults.removeObject(forKey: "UserName")
        
        
         })
    }

    

}
